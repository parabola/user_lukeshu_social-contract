The Parabola Social Contract is the commitment that we, the Parabola project,
make to the Free Software community in general and our users in particular.
It is because of this that our social contract will always follow
the philosophy of Free knowledge.
All amendments to this social contract must be faithful to the spirit of
the [Free Software Movement](https://www.fsf.org/about/what-is-free-software).

1.  **Parabola is Free Software**: Products of the Parabola project follow
    the [GNU Free System Distribution Guidelines](http://www.gnu.org/distros/free-system-distribution-guidelines.html).
    They do not include or recommend non-Free software or documentation and
    do not provide any type of support for the installation or
    execution of non-Free software. This includes:
    proprietary software, binary only firmware or binary blobs.
2.  **Parabola is Free Culture**: All documentation and cultural works
    included in products of the Parabola project are [Free Culture](http://freedomdefined.org/Definition),
    with the exceptions of:
    [works stating a viewpoint](http://www.gnu.org/licenses/license-list.html#OpinionLicenses),
    [invariant sections](https://www.gnu.org/licenses/fdl-howto-opt#SEC1) and
    [cover texts](https://www.gnu.org/licenses/fdl-howto-opt#SEC2).
    All documentation and cultural works created by or for Parabola are
    Free Culture, with no exceptions.
3.  **Parabola and other distributions**: Parabola's objective is to
    support the Free Software Movement,
    so we only need to compete against non-Free software;
    other Free Software projects are to be cooperated with, not competed with.
    Parabola strives to support other Free Software projects as best we
    can and any information from our project is available for
    anybody who needs it.
    That includes our packages and repositories.
4.  **Parabola and its community**: Parabola's community is
    [democratic in its essence](http://consensusdecisionmaking.org/) and
    [adhocratic](https://wiki.parabola.nu/Adhocracy) it its form,
    so the community is to be included whenever there is a need to
    make a decision.
    We encourage community participation in the development of the project.
5.  **Parabola and Arch**: Parabola will produce an operating system that
    is a Free version of [Arch (the GNU/Linux distribution)](http://www.archlinux.org/)
    and possibly other Arch-based systems.
    We will provide repositories and installation media without any
    non-Free software.
    All Parabola operating systems will be backward compatible with
    the system they are based on,
    as to help liberate already working installations.
    We will respect the design philosophies of the systems ours are based on,
    to reduce friction from both developer and user viewpoints.

